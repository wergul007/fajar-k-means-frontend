import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld.vue'
import DetailUmbul from '@/components/DetailUmbul.vue'
import DetailUmbulDanPenilaian from '@/components/DetailUmbulDanPenilaian.vue'

Vue.use(Router)

let theRouter = new Router({
  linkExactActiveClass: 'uk-active',
  linkActiveClass: 'uk-active',
  routes: [
    {
      path: '/home',
      alias: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      meta: {title: 'Home'}
    },
    {
      path: '/:kriteria',
      name: 'DetailUmbul',
      component: DetailUmbul,
      meta: {title: 'Kriteria Umbul'}
    },
    {
      path: '/:kriteria/:id',
      component: DetailUmbulDanPenilaian,
      name: 'DetailUmbulDanPenilaian',
      meta: {title: 'Detail Umbul'}
    }
  ]
})
theRouter.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})
export default (theRouter)
